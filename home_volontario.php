<!DOCTYPE html>
<html lang="it">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <title>Homepage - Volontario</title>
    <!-- <link href="style.css" rel="stylesheet"> -->
  </head>
  <body>
    <?php require_once 'functions.php';
    require_once 'bootstrap.php'; ?>
    <?php sec_session_start();
    $_SESSION['volontario'] = $_POST['volontario'];
    //var_dump($_SESSION['volontario']);
    $freq = $dbh->getFreq($_SESSION['volontario']);
    $myPrel = $dbh->getMyPrel($_SESSION['volontario']);
    //var_dump($myPrel);
    //var_dump($freq); ?>
    <?php require_once 'navbar_home.php';
    require_once 'modals.php';
    //sec_session_start();?>

    <div class="container justify-content-center col-md-9">
      <hr>
        <h4 class="text-center">Home Volontario</h4>
      <hr>
      <br>
      <div class="text-center">
        <h6>Bentornato, puoi donare sangue ogni <?php echo($freq[0]['frequenzaDonazSangue']); ?> giorni e plasma ogni <?php echo($freq[0]['frequenzaDonazPlasma']); ?> giorni.</h6>
      </div>
      <hr>
      <h5 class="text-center">Visualizza i dettagli dei tuoi prelievi</h5>
      <div class="text-center">
        <br>
        <button id="btn-info" type="button" class="btn btn-outline-danger" data-toggle="modal" data-target="#modal4">Visualizza</button>
      </div>

    </div>



  </body>
</html>
