-- phpMyAdmin SQL Dump
-- version 4.9.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Creato il: Feb 13, 2020 alle 01:10
-- Versione del server: 10.4.8-MariaDB
-- Versione PHP: 7.3.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `my_manageavis`
--

-- --------------------------------------------------------

--
-- Struttura della tabella `consumi`
--

CREATE TABLE `consumi` (
  `idDettaglioPrelievo` int(11) NOT NULL,
  `idOggettoMedico` int(11) NOT NULL,
  `quantita` int(2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dump dei dati per la tabella `consumi`
--

INSERT INTO `consumi` (`idDettaglioPrelievo`, `idOggettoMedico`, `quantita`) VALUES
(1, 1, 2),
(1, 2, 1);

-- --------------------------------------------------------

--
-- Struttura della tabella `dettagli_prelievi`
--

CREATE TABLE `dettagli_prelievi` (
  `idDettaglioPrelievo` int(11) NOT NULL,
  `idPrestazione` int(2) DEFAULT NULL,
  `idSede` int(11) DEFAULT NULL,
  `numeroLocale` int(11) DEFAULT NULL,
  `data` date NOT NULL,
  `oraInizio` time NOT NULL,
  `CF` char(16) NOT NULL,
  `qta_prelevata` int(3) NOT NULL,
  `importoColazione` int(2) DEFAULT NULL,
  `note` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dump dei dati per la tabella `dettagli_prelievi`
--

INSERT INTO `dettagli_prelievi` (`idDettaglioPrelievo`, `idPrestazione`, `idSede`, `numeroLocale`, `data`, `oraInizio`, `CF`, `qta_prelevata`, `importoColazione`, `note`) VALUES
(1, 2, 1, 1, '2020-02-09', '09:20:00', 'GBBSML98A15C573G', 450, 10, 'molto bene'),
(2, 3, 1, 1, '2020-02-09', '09:30:00', 'RCCMTT97R13H294I', 500, 6, 'Prima donazione, nessun sintomo particolare.'),
(4, 3, 1, 2, '2020-02-11', '09:00:00', 'GBBSML98A15C573G', 450, 4, 'primo plasma'),
(7, 2, 1, 2, '2020-02-11', '09:55:00', 'RCCMTT97R13H294I', 400, 3, 'prova'),
(8, 3, 2, 4, '2020-02-12', '11:00:00', 'STWSTW2343BUVFID', 480, 2, 'Prossima volta è meglio presentarsi senza una colazione abbondante.'),
(9, 3, 3, 6, '2020-02-13', '09:50:00', 'WVQMPU63C02G047K', 500, 4, NULL);

-- --------------------------------------------------------

--
-- Struttura della tabella `dettagli_visite`
--

CREATE TABLE `dettagli_visite` (
  `idDettaglioVisita` int(11) NOT NULL,
  `idPrestazione` int(2) DEFAULT NULL,
  `idSede` int(3) DEFAULT NULL,
  `numeroLocale` int(3) DEFAULT NULL,
  `data` date NOT NULL,
  `oraInizio` time NOT NULL,
  `CF` char(16) DEFAULT NULL,
  `esito` int(1) NOT NULL,
  `emoglobina` int(3) DEFAULT NULL,
  `note` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dump dei dati per la tabella `dettagli_visite`
--

INSERT INTO `dettagli_visite` (`idDettaglioVisita`, `idPrestazione`, `idSede`, `numeroLocale`, `data`, `oraInizio`, `CF`, `esito`, `emoglobina`, `note`) VALUES
(1, 1, 1, 1, '2020-02-09', '09:15:00', 'GBBSML98A15C573G', 1, 17, 'Tutto ok');

-- --------------------------------------------------------

--
-- Struttura della tabella `locali`
--

CREATE TABLE `locali` (
  `numeroLocale` int(4) NOT NULL,
  `tipoLocale` varchar(30) NOT NULL,
  `idSede` int(3) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dump dei dati per la tabella `locali`
--

INSERT INTO `locali` (`numeroLocale`, `tipoLocale`, `idSede`) VALUES
(1, 'Ambulatorio', 1),
(2, 'Sala Donazioni', 1),
(3, 'Ambulatorio', 2),
(4, 'Sala Donazioni', 2),
(5, 'Ambulatorio', 3),
(6, 'Sala Donazioni', 3);

-- --------------------------------------------------------

--
-- Struttura della tabella `oggetti_medici`
--

CREATE TABLE `oggetti_medici` (
  `idOggettoMedico` int(3) NOT NULL,
  `nome` varchar(30) NOT NULL,
  `costoUnitario` decimal(3,2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dump dei dati per la tabella `oggetti_medici`
--

INSERT INTO `oggetti_medici` (`idOggettoMedico`, `nome`, `costoUnitario`) VALUES
(1, 'Siringa', '0.10'),
(2, 'Sacca 0,5L', '0.05'),
(3, 'Soluzione fisiologica 0,5L', '0.50');

-- --------------------------------------------------------

--
-- Struttura della tabella `personale`
--

CREATE TABLE `personale` (
  `CF` char(16) NOT NULL,
  `ruolo` varchar(30) NOT NULL,
  `nome` varchar(30) NOT NULL,
  `cognome` varchar(30) NOT NULL,
  `dataDiNascita` date NOT NULL,
  `telefono` bigint(10) NOT NULL,
  `compensoOrario` int(2) NOT NULL,
  `idSede` int(3) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dump dei dati per la tabella `personale`
--

INSERT INTO `personale` (`CF`, `ruolo`, `nome`, `cognome`, `dataDiNascita`, `telefono`, `compensoOrario`, `idSede`) VALUES
('CTQSCR40D57Z132L', 'Medico', 'Kostas', 'Manolas', '1992-10-30', 3321457342, 20, 3),
('KDBKDBKDBKDBKDBK', 'Infermiere', 'Kevin', 'De Bruyne', '1991-10-30', 1010101010, 15, 1),
('MBBPRG57H41D133Y', 'Infermiere', 'Miralem', 'Pjanic', '1990-10-30', 3335555555, 12, 3),
('RYHWJL29L55A737U', 'Medico', 'Riyad', 'Mahrez', '1991-10-30', 3114753800, 20, 2),
('THDZHJ43D56E131V', 'Infermiere', 'Dusan', 'Tadic', '1989-10-30', 3124574445, 12, 2),
('TRTFPP98R30C573G', 'Medico', 'Filippo', 'Tartagni', '1997-10-30', 3270140970, 20, 1),
('TRTSTF68L14C573G', 'Infermiere', 'Stefano', 'Tartagni', '1968-07-14', 3337771112, 10, 1),
('ZXEPSW71A50C038Y', 'Infermiere', 'Hakim', 'Ziyech', '1997-10-30', 3331822000, 10, 1);

-- --------------------------------------------------------

--
-- Struttura della tabella `prestazioni_mediche`
--

CREATE TABLE `prestazioni_mediche` (
  `idPrestazione` int(2) NOT NULL,
  `nomePrestazione` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dump dei dati per la tabella `prestazioni_mediche`
--

INSERT INTO `prestazioni_mediche` (`idPrestazione`, `nomePrestazione`) VALUES
(1, 'Visita preliminare'),
(2, 'Prelievo Sangue'),
(3, 'Prelievo Plasma');

-- --------------------------------------------------------

--
-- Struttura della tabella `sedi`
--

CREATE TABLE `sedi` (
  `idSede` int(3) NOT NULL,
  `nome` varchar(30) NOT NULL,
  `via` varchar(30) NOT NULL,
  `numero` int(4) NOT NULL,
  `citta` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dump dei dati per la tabella `sedi`
--

INSERT INTO `sedi` (`idSede`, `nome`, `via`, `numero`, `citta`) VALUES
(1, 'Avis San Mauro', 'via borgo madonna rossa', 333, 'San Mauro Pascoli'),
(2, 'Avis Savignano', 'via borgo madonna rossa', 444, 'Savignano sul Rubicone'),
(3, 'Avis Gambettola', 'via Fleming', 12, 'Gambettola');

-- --------------------------------------------------------

--
-- Struttura della tabella `turni`
--

CREATE TABLE `turni` (
  `CF` char(16) DEFAULT NULL,
  `idSede` int(11) NOT NULL,
  `numeroLocale` int(11) NOT NULL,
  `data` date NOT NULL,
  `orarioInizio` time NOT NULL,
  `orarioFine` time NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dump dei dati per la tabella `turni`
--

INSERT INTO `turni` (`CF`, `idSede`, `numeroLocale`, `data`, `orarioInizio`, `orarioFine`) VALUES
('TRTFPP98R30C573G', 1, 1, '2020-02-09', '09:00:00', '12:00:00'),
('TRTFPP98R30C573G', 1, 1, '2020-02-12', '08:00:00', '09:00:00'),
('TRTFPP98R30C573G', 1, 2, '2020-02-10', '08:00:00', '12:00:00'),
('TRTFPP98R30C573G', 1, 2, '2020-02-11', '08:00:00', '10:00:00'),
('RYHWJL29L55A737U', 2, 4, '2020-02-12', '08:00:00', '13:00:00'),
('CTQSCR40D57Z132L', 3, 6, '2020-02-13', '08:00:00', '10:00:00');

-- --------------------------------------------------------

--
-- Struttura della tabella `turni_infermieri`
--

CREATE TABLE `turni_infermieri` (
  `CF` char(16) NOT NULL,
  `idSede` int(3) DEFAULT NULL,
  `numeroLocale` int(3) DEFAULT NULL,
  `data` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dump dei dati per la tabella `turni_infermieri`
--

INSERT INTO `turni_infermieri` (`CF`, `idSede`, `numeroLocale`, `data`) VALUES
('TRTSTF68L14C573G', 1, 1, '2020-02-09'),
('TRTSTF68L14C573G', 1, 2, '2020-02-10'),
('THDZHJ43D56E131V', 2, 4, '2020-02-12'),
('MBBPRG57H41D133Y', 3, 6, '2020-02-13');

-- --------------------------------------------------------

--
-- Struttura della tabella `volontari`
--

CREATE TABLE `volontari` (
  `CF` char(16) NOT NULL,
  `nome` varchar(30) NOT NULL,
  `cognome` varchar(30) NOT NULL,
  `dataDiNascita` date NOT NULL,
  `telefono` bigint(10) NOT NULL,
  `idSede` int(3) DEFAULT NULL,
  `gruppoSanguigno` char(3) NOT NULL,
  `frequenzaDonazSangue` int(3) NOT NULL,
  `frequenzaDonazPlasma` int(3) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dump dei dati per la tabella `volontari`
--

INSERT INTO `volontari` (`CF`, `nome`, `cognome`, `dataDiNascita`, `telefono`, `idSede`, `gruppoSanguigno`, `frequenzaDonazSangue`, `frequenzaDonazPlasma`) VALUES
('FZNMDI72A13E481E', 'Gianluigi', 'Buffon', '1978-10-30', 3471111111, 1, 'A+', 90, 30),
('GBBSML98A15C573G', 'Samuele', 'Gobbi', '1998-01-15', 3333333333, 1, '0-', 90, 30),
('RCCMTT97R13H294I', 'Mattia', 'Ricci', '1997-02-13', 3334445550, 1, 'AB+', 90, 30),
('RSSGNN65H30H294I', 'Gianni', 'Rossi', '1965-10-30', 3278956453, 1, 'A+', 180, 60),
('STWSTW2343BUVFID', 'Stephan', 'El Sharaawy', '1992-10-30', 3260138643, 2, '0-', 90, 30),
('WVQMPU63C02G047K', 'Edin', 'Dzeko', '1984-02-02', 3331010980, 3, 'AB+', 90, 60);

--
-- Indici per le tabelle scaricate
--

--
-- Indici per le tabelle `consumi`
--
ALTER TABLE `consumi`
  ADD PRIMARY KEY (`idDettaglioPrelievo`,`idOggettoMedico`),
  ADD KEY `idOggettoMedico` (`idOggettoMedico`);

--
-- Indici per le tabelle `dettagli_prelievi`
--
ALTER TABLE `dettagli_prelievi`
  ADD PRIMARY KEY (`idDettaglioPrelievo`),
  ADD UNIQUE KEY `CF` (`CF`,`oraInizio`,`numeroLocale`),
  ADD UNIQUE KEY `idSede` (`idSede`,`numeroLocale`,`data`,`oraInizio`),
  ADD KEY `idPrestazione` (`idPrestazione`);

--
-- Indici per le tabelle `dettagli_visite`
--
ALTER TABLE `dettagli_visite`
  ADD PRIMARY KEY (`idDettaglioVisita`),
  ADD UNIQUE KEY `CF` (`CF`,`oraInizio`,`numeroLocale`),
  ADD UNIQUE KEY `idSede` (`idSede`,`numeroLocale`,`data`,`oraInizio`),
  ADD KEY `idPrestazione` (`idPrestazione`);

--
-- Indici per le tabelle `locali`
--
ALTER TABLE `locali`
  ADD PRIMARY KEY (`numeroLocale`,`idSede`),
  ADD KEY `idSede` (`idSede`);

--
-- Indici per le tabelle `oggetti_medici`
--
ALTER TABLE `oggetti_medici`
  ADD PRIMARY KEY (`idOggettoMedico`);

--
-- Indici per le tabelle `personale`
--
ALTER TABLE `personale`
  ADD PRIMARY KEY (`CF`),
  ADD KEY `idSede` (`idSede`);

--
-- Indici per le tabelle `prestazioni_mediche`
--
ALTER TABLE `prestazioni_mediche`
  ADD PRIMARY KEY (`idPrestazione`);

--
-- Indici per le tabelle `sedi`
--
ALTER TABLE `sedi`
  ADD PRIMARY KEY (`idSede`);

--
-- Indici per le tabelle `turni`
--
ALTER TABLE `turni`
  ADD PRIMARY KEY (`idSede`,`numeroLocale`,`data`),
  ADD UNIQUE KEY `CF` (`CF`,`data`);

--
-- Indici per le tabelle `turni_infermieri`
--
ALTER TABLE `turni_infermieri`
  ADD PRIMARY KEY (`CF`,`data`),
  ADD UNIQUE KEY `CF` (`CF`,`data`),
  ADD KEY `turni_infermieri_ibfk_2` (`idSede`,`numeroLocale`);

--
-- Indici per le tabelle `volontari`
--
ALTER TABLE `volontari`
  ADD PRIMARY KEY (`CF`),
  ADD KEY `idSede` (`idSede`);

--
-- AUTO_INCREMENT per le tabelle scaricate
--

--
-- AUTO_INCREMENT per la tabella `dettagli_prelievi`
--
ALTER TABLE `dettagli_prelievi`
  MODIFY `idDettaglioPrelievo` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT per la tabella `dettagli_visite`
--
ALTER TABLE `dettagli_visite`
  MODIFY `idDettaglioVisita` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT per la tabella `locali`
--
ALTER TABLE `locali`
  MODIFY `numeroLocale` int(4) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT per la tabella `oggetti_medici`
--
ALTER TABLE `oggetti_medici`
  MODIFY `idOggettoMedico` int(3) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT per la tabella `sedi`
--
ALTER TABLE `sedi`
  MODIFY `idSede` int(3) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- Limiti per le tabelle scaricate
--

--
-- Limiti per la tabella `dettagli_prelievi`
--
ALTER TABLE `dettagli_prelievi`
  ADD CONSTRAINT `dettagli_prelievi_ibfk_1` FOREIGN KEY (`idPrestazione`) REFERENCES `prestazioni_mediche` (`idPrestazione`),
  ADD CONSTRAINT `dettagli_prelievi_ibfk_2` FOREIGN KEY (`CF`) REFERENCES `volontari` (`CF`),
  ADD CONSTRAINT `dettagli_prelievi_ibfk_3` FOREIGN KEY (`idSede`,`numeroLocale`,`data`) REFERENCES `turni` (`idSede`, `numeroLocale`, `data`);

--
-- Limiti per la tabella `dettagli_visite`
--
ALTER TABLE `dettagli_visite`
  ADD CONSTRAINT `dettagli_visite_ibfk_1` FOREIGN KEY (`idPrestazione`) REFERENCES `prestazioni_mediche` (`idPrestazione`),
  ADD CONSTRAINT `dettagli_visite_ibfk_2` FOREIGN KEY (`CF`) REFERENCES `volontari` (`CF`),
  ADD CONSTRAINT `dettagli_visite_ibfk_3` FOREIGN KEY (`idSede`,`numeroLocale`,`data`) REFERENCES `turni` (`idSede`, `numeroLocale`, `data`);

--
-- Limiti per la tabella `locali`
--
ALTER TABLE `locali`
  ADD CONSTRAINT `locali_ibfk_1` FOREIGN KEY (`idSede`) REFERENCES `sedi` (`idSede`);

--
-- Limiti per la tabella `personale`
--
ALTER TABLE `personale`
  ADD CONSTRAINT `personale_ibfk_1` FOREIGN KEY (`idSede`) REFERENCES `sedi` (`idSede`) ON DELETE SET NULL ON UPDATE NO ACTION;

--
-- Limiti per la tabella `turni`
--
ALTER TABLE `turni`
  ADD CONSTRAINT `turni_ibfk_1` FOREIGN KEY (`CF`) REFERENCES `personale` (`CF`) ON DELETE SET NULL ON UPDATE NO ACTION,
  ADD CONSTRAINT `turni_ibfk_2` FOREIGN KEY (`idSede`,`numeroLocale`) REFERENCES `locali` (`idSede`, `numeroLocale`);

--
-- Limiti per la tabella `turni_infermieri`
--
ALTER TABLE `turni_infermieri`
  ADD CONSTRAINT `turni_infermieri_ibfk_1` FOREIGN KEY (`CF`) REFERENCES `personale` (`CF`),
  ADD CONSTRAINT `turni_infermieri_ibfk_2` FOREIGN KEY (`idSede`,`numeroLocale`) REFERENCES `locali` (`idSede`, `numeroLocale`) ON DELETE NO ACTION ON UPDATE SET NULL;

--
-- Limiti per la tabella `volontari`
--
ALTER TABLE `volontari`
  ADD CONSTRAINT `volontari_ibfk_1` FOREIGN KEY (`idSede`) REFERENCES `sedi` (`idSede`) ON DELETE SET NULL ON UPDATE NO ACTION;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
