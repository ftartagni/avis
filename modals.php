<!-- <button id="btn-info" type="button" class="btn btn-outline-primary" data-toggle="modal" data-target="#modal1">Dettagli Prelievi</button>
<button id="btn-info" type="button" class="btn btn-outline-danger" data-toggle="modal" data-target="#modal2">Visualizza Turni</button> -->

<div id="modal1" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="modalA" aria-hidden="true">
    <div class="modal-dialog modal-xl modal-dialog-centered modal-dialog-scrollable" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="modalA">Visualizzazione prelievi sede numero: <?php echo($dettagliPrelievi[0]['idSede']); ?></h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">


              <div class="table-responsive-xl">
                  <table class="table table-striped">
                      <thead class="thead-dark">
                        <tr>
                          <th id="idP" width="5%" scope="col">ID</th>
                          <th id="data" width="10%" scope="col">DATA</th>
                          <th id="nome" width="10%" scope="col">NOME</th>
                          <th id="cognome" width="10%" scope="col">COGNOME</th>
                          <th id="tipo" width="10%" scope="col">TIPO</th>
                          <th id="qtà" width="5%" scope="col">QTà</th>
                          <th id="note" width="30%" scope="col">NOTE</th>
                        </tr>
                      </thead>
                      <tbody>
                        <tr>
                          <?php foreach ($dettagliPrelievi as $DP): ?>
                            <td headers="idP"><?php echo($DP['idDettaglioPrelievo']); ?></td>
                            <td headers="data"><?php echo($DP['data']); ?></td>
                            <td><?php echo($DP['nome']); ?></td>
                            <td><?php echo($DP['cognome']); ?></td>
                            <td><?php echo($DP['nomePrestazione']); ?></td>
                            <td><?php echo($DP['qta_prelevata']) ?></td>
                            <td><?php echo($DP['note']) ?></td>
                        </tr>
                          <?php endforeach; ?>
                      </tbody>
                  </table>
              </div>


            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Chiudi</button>
            </div>
        </div>
    </div>
</div>


<div id="modal2" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="modalB" aria-hidden="true">
    <div class="modal-dialog modal-xl modal-dialog-centered modal-dialog-scrollable" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="modalA">Visualizzazione turni medici sede numero: <?php echo($turni_m[0]['idSede']); ?></h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">


              <div class="table-responsive-xl">
                  <table class="table table-striped">
                      <thead class="thead-dark">
                        <tr>
                          <th id="idP" width="15%" scope="col">NUM. LOCALE</th>
                          <th id="data>">DATA</th>
                          <th>NOME</th>
                          <th>COGNOME</th>
                          <th>ORA INIZIO</th>
                          <th>ORA FINE</th>

                        </tr>
                      </thead>
                      <tbody>
                        <tr>
                          <?php foreach ($turni_m as $tm): ?>
                            <td headers="idP"><?php echo($tm['numeroLocale']); ?></td>
                            <td headers="data"><?php echo($tm['data']); ?></td>
                            <td><?php echo($tm['nome']); ?></td>
                            <td><?php echo($tm['cognome']); ?></td>
                            <td><?php echo($tm['orarioInizio']) ?></td>
                            <td><?php echo($tm['orarioFine']) ?></td>
                        </tr>
                          <?php endforeach; ?>
                      </tbody>
                  </table>
              </div>


            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Chiudi</button>
            </div>
        </div>
    </div>
</div>

<div id="modal3" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="modalB" aria-hidden="true">
    <div class="modal-dialog modal-xl modal-dialog-centered modal-dialog-scrollable" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="modalA">Visualizzazione turni infermieri sede numero: <?php echo($turni_i[0]['idSede']); ?></h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">


              <div class="table-responsive-xl">
                  <table class="table table-striped">
                      <thead class="thead-dark">
                        <tr>
                          <th id="data>">DATA</th>
                          <th>NOME</th>
                          <th>COGNOME</th>
                          <th>ORA INIZIO</th>
                          <th>ORA FINE</th>

                        </tr>
                      </thead>
                      <tbody>
                        <tr>
                          <?php foreach ($turni_i as $ti): ?>
                            <td headers="data"><?php echo($ti['data']); ?></td>
                            <td><?php echo($ti['nome']); ?></td>
                            <td><?php echo($ti['cognome']); ?></td>
                            <td><?php echo($ti['orarioInizio']) ?></td>
                            <td><?php echo($ti['orarioFine']) ?></td>
                        </tr>
                          <?php endforeach; ?>
                      </tbody>
                  </table>
              </div>


            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Chiudi</button>
            </div>
        </div>
    </div>
</div>


<div id="modal4" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="modalA" aria-hidden="true">
    <div class="modal-dialog modal-xl modal-dialog-centered modal-dialog-scrollable" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="modalA">Visualizzazione dei tuoi prelievi:</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">

              <?php  if(empty($myPrel)){
                ?> <h4 class="text-center"> Non hai ancora eseguito prelievi.</h4>
              <?php  } else { ?>
              <div class="table-responsive-xl">
                  <table class="table table-striped">
                      <thead class="thead-dark">
                        <tr>
                          <th id="idP" width="5%" scope="col">ID</th>
                          <th id="data" width="15%" scope="col">DATA</th>
                          <th id="tipo" width="15%" scope="col">TIPO</th>
                          <th id="qtà" scope="col">Q.TÀ PRELEVATA(mL)</th>
                          <th id="note" width="30%" scope="col">NOTE</th>
                        </tr>
                      </thead>
                      <tbody>
                        <tr>
                          <?php foreach ($myPrel as $myP): ?>
                            <td headers="idP"><?php echo($myP['idDettaglioPrelievo']); ?></td>
                            <td headers="data"><?php echo($myP['data']); ?></td>
                            <td><?php echo($myP['nomePrestazione']); ?></td>
                            <td><?php echo($myP['qta_prelevata']) ?></td>
                            <td><?php echo($myP['note']) ?></td>
                        </tr>
                          <?php endforeach; ?>
                      </tbody>
                  </table>
              </div>
            <?php } ?>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Chiudi</button>
            </div>
        </div>
    </div>
</div>
