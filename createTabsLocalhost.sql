create table sedi (
	idSede int(3) not null AUTO_INCREMENT,
	nome varchar(30) not null,
	via varchar(30) not null,
	numero int(4) not null,
	città varchar(30) not null,
  primary key idSede
)ENGINE=InnoDB;

create table locali(
	numeroLocale int(4) not null AUTO_INCREMENT,
	tipoLocale varchar(30) not null,
	idSede int(3) not null,
  foreign key (idSede)
  references sedi (idSede)
	primary key (numeroLocale, idSede)
)ENGINE=InnoDB;

create table personale (
	CF char(16) not null primary key,
  matricola int(3) not null,
  ruolo varchar(30) not null,
  nome varchar(30) not null,
  cognome varchar(30) not null,
  dataDiNascita date not null,
  telefono bigint(10) not null,
  compensoOrario int(2) not null,
  idSede int(3) not null,
   foreign key (idSede) references sedi (idSede)
   on delete set null
   on update no action,
  unique(idSede, matricola)
)ENGINE=InnoDB;

	create table volontari (
		CF char(16) not null primary key,
	  nome varchar(30) not null,
	  cognome varchar(30) not null,
	  dataDiNascita date not null,
	  telefono bigint(10) not null,
	  idSede int(3) not null,
    foreign key (idSede) references sedi (idSede)
			on delete set null
      on update no action,
		gruppoSanguigno char(3) not null,
		frequenzaDonazSangue int(3) not null,
		frequenzaDonazPlasma int(3) not null
	)ENGINE=InnoDB;


create table prestazioni_mediche(
		idPrestazione int(2) not null primary key,
		nomePrestazione varchar(30) not null
	)ENGINE=InnoDB;

	create table turni(
	  CF char(16),
	   foreign key (CF) references personale (CF)
	    on delete set null
	    on update no action,
	  idSede int(3),
	  numeroLocale int(3),
	  data date not null,
	  orarioInizio time not null,
	  orarioFine time not null,
	  primary key (idSede, numeroLocale, data),
		foreign key (idSede, numeroLocale) references locali (idSede, numeroLocale)
		on delete set null
		on update no action,
	  unique(CF, data)
	)ENGINE=InnoDB;

create table turni_infermieri(
  CF char(16) not null,
  foreign key (CF) references personale (CF),
  idSede int(3),
  numeroLocale int(3),
  data date not null,
  primary key (CF, data),
	foreign key (idSede, numeroLocale) references locali (idSede, numeroLocale),

  unique(CF, data)
)ENGINE=InnoDB;

create table dettagli_prelievi(
  idDettaglioPrelievo int not null AUTO_INCREMENT,
  PRIMARY key (idDettaglioPrelievo),
  idPrestazione int(2),
  foreign key (idPrestazione) references prestazioni_mediche (idPrestazione),
  idSede int,
  numeroLocale int,
  data date not null,
  oraInizio time not null,
  CF char(16) not null,
  foreign key (CF) references volontari (CF),
  importoColazione int(2),
  note varchar(255),
  foreign key (idSede, numeroLocale, data) references turni (idSede, numeroLocale, data),
  unique (CF, oraInizio, numeroLocale),
  unique (idSede, numeroLocale, data, oraInizio)
)ENGINE=InnoDB;

create table dettagli_visite(
  idDettaglioVisita int not null AUTO_INCREMENT,
	primary key (idDettaglioVisita),
  idPrestazione int(2),
  foreign key (idPrestazione) references prestazioni_mediche (idPrestazione),
  idSede int(3),
  numeroLocale int(3),
  data date not null,
  oraInizio time not null,
  CF char(16),
  foreign key (CF) references volontari (CF),
  esito int(1) not null,
  emoglobina int(3),
  note varchar(255),
  foreign key (idSede, numeroLocale, data) references turni (idSede, numeroLocale, data),
  unique (CF, oraInizio, numeroLocale),
  unique (idSede, numeroLocale, data, oraInizio)
)ENGINE=InnoDB;

create table oggetti_medici(
  idOggettoMedico int(3) not null primary key,
  nome varchar(30) not null,
  costoUnitario int not null
)ENGINE=InnoDB;

create table consumi(
  idDettaglioPrelievo int,
  foreign key (idDettaglioPrelievo) references dettagli_prelievi (idDettaglioPrelievo),
  idOggettoMedico int(3),
  foreign key (idOggettoMedico) references oggetti_medici (idOggettoMedico),
  quantità int(2) not null,
  primary key (idDettaglioPrelievo, idOggettoMedico)
)ENGINE=InnoDB;
