<!DOCTYPE html>
<html lang="it">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <title>Homepage - Avis</title>
    <!-- <link href="style.css" rel="stylesheet"> -->
  </head>
  <body>
    <?php require_once 'functions.php'; ?>
    <?php require_once 'navbar_home.php';
    require_once 'bootstrap.php';
    sec_session_start();
    $sedi = $dbh->getSedi();
    $medici = $dbh->getMedici();
    $volontari = $dbh->getVolontari();
    // $_POST['sede'] = 1;
    // $dettagliPrelievi = $dbh->getDettagliPrelievi(1);
    // $turni_m = $dbh->getTurniMedici(1);
    // $turni_i = $dbh->getTurniInfermieri(1);
    // var_dump($dettagliPrelievi);
    require_once 'modals.php';
    // var_dump($_SESSION);

    // var_dump($sedi);
    ?>
    <style media="screen">
      button{
        margin-top: 4%;
      }
    </style>

    <div class="container justify-content-center col-md-6">
      <hr class="upRegister">
      <h4 class="text-center">Login Amministratore </h4>
      <!-- <hr class="downRegister"> -->
      <div class="container justify-content-center col-md-6">
        <div class="form-group">
          <form id="adminnn" action="home_admin.php" method="post">
          <label for="admins">Seleziona sede</label>
          <select name="admins"  class="form-control">
            <?php foreach($sedi as $sede): ?>
              <option value="<?php echo $sede['idSede'];?>"><?php echo $sede['nome']; ?></option>
            <?php endforeach; ?>
          </select>
          <div class="container justify-content-center col-md-6">
         <button class="btn btn-primary" type="submit">Login</button>
       </div>
          </form>
        </div>
      </div>
      <hr class="upRegister">
      <h4 class="text-center">Login Medico </h4>
      <!-- <hr class="downRegister"> -->
      <div class="form-group">
        <form id="medico" action="home_medico.php" method="post">
          <div class="container justify-content-center col-md-6">
          <label for="medico">Codice Fiscale</label>
          <select name="medico"  class="form-control">
            <?php foreach($medici as $medico): ?>
              <option value="<?php echo $medico['CF'];?>"><?php echo $medico['CF'];//$medico['nome']." ".$medico['cognome']; ?></option>
            <?php endforeach; ?>
          </select>
          <div class="container justify-content-center col-md-6">
            <button class="btn btn-primary" type="submit">Login</button>
          </div>
        </div>
       </form>
      </div>
      <hr class="upRegister">
      <h4 class="text-center">Login Volontario</h4>
      <!-- <hr class="downRegister"> -->
        <div class="form-group">
          <form id="volontario" action="home_volontario.php" method="post">
            <div class="container justify-content-center col-md-6">
          <label for="volontario">Codice Fiscale</label>
          <select name="volontario"  class="form-control">
            <?php foreach($volontari as $volontario): ?>
              <option value="<?php echo $volontario['CF'];?>"><?php echo $volontario['CF'];?></option>
            <?php endforeach; ?>
          </select>
          <div class="container justify-content-center col-md-6">
            <button class="btn btn-primary" type="submit">Login</button>
          </div>
      </div>
      </form>
      </div>
      <hr class="downRegister">

  </body>
</html>
