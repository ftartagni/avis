<?php
class DatabaseHelper{
    private $db;

    public function __construct($servername, $username, $password, $dbname){
        $this->db = new mysqli($servername, $username, $password, $dbname);
        if ($this->db->connect_error) {
            die("Connection failed: " . $db->connect_error);
        }
    }

    public function getSedi(){      //ok
        $stmt = $this->db->prepare("SELECT idSede, nome FROM sedi");
        $stmt->execute();
        $result = $stmt->get_result();

        return $result->fetch_all(MYSQLI_ASSOC);
    }

    public function getMedici(){      //ok
        $stmt = $this->db->prepare("SELECT CF, nome, cognome FROM personale WHERE ruolo = 'Medico'");
        $stmt->execute();
        $result = $stmt->get_result();

        return $result->fetch_all(MYSQLI_ASSOC);
    }

    public function getVolontari(){    //ok
        $stmt = $this->db->prepare("SELECT CF, nome, cognome FROM volontari");
        $stmt->execute();
        $result = $stmt->get_result();

        return $result->fetch_all(MYSQLI_ASSOC);
    }

    public function insert_volontario(){    //ok
    $stmt = $this->db->prepare('INSERT INTO volontari (nome, cognome, CF, telefono, dataDiNascita,
       idSede, gruppoSanguigno, frequenzaDonazSangue, frequenzaDonazPlasma) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)');

      $stmt->bind_param('sssisisii', $_POST['nome'], $_POST['cognome'], $_POST['CF'], $_POST['tel'],
                        $_POST['birthdate'], $_POST['sede'], $_POST['gruppo'],
                        $_POST['frequenzaDonazSangue'], $_POST['frequenzaDonazPlasma']);
      $stmt->execute();
    }

    public function insert_personale(){     //ok
      $stmt = $this->db->prepare('INSERT INTO personale (ruolo, nome, cognome, CF, telefono, dataDiNascita,
         compensoOrario, idSede) VALUES (?, ?, ?, ?, ?, ?, ?, ?)');
      $stmt->bind_param('ssssisis', $_POST['ruolo'], $_POST['nome'], $_POST['cognome'], $_POST['CF'], $_POST['tel'],
                           $_POST['birthdate'], $_POST['compenso'], $_POST['sede']);
      $stmt->execute();
    }

    public function getTurniMedici($idSede){   //ok
      $stmt = $this->db->prepare('SELECT t.CF, p.nome, p.cognome, t.data, t.orarioInizio, t.orarioFine, t.numeroLocale, p.idSede
                                  FROM personale p, turni t WHERE p.CF = t.CF AND p.idSede = ?');
      $stmt->bind_param('i', $idSede);
      $stmt->execute();
      $result = $stmt->get_result();

      return $result->fetch_all(MYSQLI_ASSOC);
    }

    public function getDettagliPrelievi($idSede){   //ok
      $stmt = $this->db->prepare('SELECT d.idDettaglioPrelievo, d.idPrestazione, d.data, d.oraInizio, d.qta_prelevata,
              d.note, v.nome, v.cognome, p.nomePrestazione, d.idSede FROM dettagli_prelievi d, volontari v, prestazioni_mediche p
              WHERE d.CF = v.CF AND d.idSede = v.idSede AND d.idSede = ? AND d.idPrestazione = p.idPrestazione');

      $stmt->bind_param('i', $idSede);
      $stmt->execute();
      $result = $stmt->get_result();

      return $result->fetch_all(MYSQLI_ASSOC);
    }

    public function getTurniInfermieri($idSede){
      $stmt = $this->db->prepare('SELECT ti.CF, p.nome, p.cognome, t.idSede, t.data, t.orarioInizio, t.orarioFine, t.CF
        FROM personale p, turni t, turni_infermieri ti
        WHERE p.CF = ti.CF
        AND ti.idSede = t.idSede
        AND ti.numeroLocale = ti.numeroLocale
        AND ti.data = t.data
        AND p.idSede = ?');

      $stmt->bind_param('i', $idSede);
      $stmt->execute();
      $result = $stmt->get_result();

      return $result->fetch_all(MYSQLI_ASSOC);
    }

    public function getSpeseOgg($month, $idSede){
      $stmt = $this->db->prepare('SELECT SUM(o.costoUnitario * c.quantita) AS spese_oggetti
       FROM consumi c, oggetti_medici o, dettagli_prelievi d
       WHERE MONTH(d.data) = ?
       AND c.idDettaglioPrelievo = d.idDettaglioPrelievo
       AND o.idOggettoMedico = c.idOggettoMedico
       AND d.idSede = ?');

      $stmt->bind_param('ii', $month, $idSede);
      $stmt->execute();
      $result = $stmt->get_result();

      return $result->fetch_all(MYSQLI_ASSOC);
    }

    public function getFreq($CF){
      $stmt = $this->db->prepare('SELECT v.frequenzaDonazPlasma, v.frequenzaDonazSangue
       FROM volontari v
       WHERE v.CF = ?');

      $stmt->bind_param('s', $CF);
      $stmt->execute();
      $result = $stmt->get_result();

      return $result->fetch_all(MYSQLI_ASSOC);
    }

    public function getTotPrel($idSede){
      $stmt = $this->db->prepare('SELECT COUNT(*) as tot
       FROM dettagli_prelievi d, sedi s
       WHERE d.idSede = s.idSede
       AND YEAR(d.data) = YEAR(CURDATE())
       AND d.idSede = ?');

      $stmt->bind_param('i', $idSede);
      $stmt->execute();
      $result = $stmt->get_result();

      return $result->fetch_all(MYSQLI_ASSOC);
    }

    public function getMyPrel($CF){
      $stmt = $this->db->prepare('SELECT d.idDettaglioPrelievo, d.idPrestazione, d.data, d.oraInizio, d.qta_prelevata,
             d.note, v.nome, v.cognome, p.nomePrestazione
       FROM dettagli_prelievi d, volontari v, prestazioni_mediche p
       WHERE d.CF = v.CF
       AND d.idSede = v.idSede
       AND d.idPrestazione = p.idPrestazione
       AND v.CF = ?');

      $stmt->bind_param('s', $CF);
      $stmt->execute();
      $result = $stmt->get_result();

      return $result->fetch_all(MYSQLI_ASSOC);

    }

    public function insertPrel(){
      $stmt = $this->db->prepare('INSERT INTO dettagli_prelievi (qta_prelevata, CF, data, oraInizio, idSede, idPrestazione,
                                 numeroLocale, note, importoColazione) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)');

      // $qtà = intval($_POST['qtà_prel']);
      // $sede = intval($_POST['sede']);
      // $tipo = intval($_POST['tipo']);
      // $locale = intval($_POST['locale']);
      // $imp = intval($_POST['imp']);
        $stmt->bind_param('isssiiisi', $_POST['qta_prel'], $_POST['volontario'], $_POST['date'], $_POST['ora'],
              $_POST['sede'], $_POST['tipo'], $_POST['locale'], $_POST['note'], $_POST['imp']);
        // $stmt->bind_param('isssiiisi', $qtà, $_POST['volontario'], $_POST['date'], $_POST['ora'],
        //       $sede, $tipo, $locale, $_POST['note'], $imp);
        $stmt->execute();
    }

    public function insertTurno(){
      $stmt = $this->db->prepare('INSERT INTO turni (CF, idSede, numeroLocale, data, orarioInizio, orarioFine) VALUES (?, ?, ?, ?, ?, ?)');

        $stmt->bind_param('siisss', $_SESSION['medico'], $_POST['sede'], $_POST['locale'], $_POST['date'], $_POST['oraI'], $_POST['oraF']);
        $stmt->execute();
    }
}

?>
