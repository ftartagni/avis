<!DOCTYPE html>
<html lang="it">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <title>Homepage - Admin</title>
    <!-- <link href="style.css" rel="stylesheet"> -->
  </head>
  <body>
    <?php require_once 'functions.php';
    require_once 'bootstrap.php';?>
    <?php sec_session_start();
    //if(empty($_SESSION['admin'])){
    if(!empty($_POST['admins'])){
      $_SESSION['admin'] = $_POST['admins'];
    }
    //} else {}
    //var_dump($_SESSION['admin']); ?>
    <?php require_once 'navbar_home.php';
    //sec_session_start();
    $turni_i = $dbh->getTurniInfermieri($_SESSION['admin']);
    //var_dump($turni_i);
    $turni_m = $dbh->getTurniMedici($_SESSION['admin']);
    $numPrel = $dbh->getTotPrel($_SESSION['admin']);
    $dettagliPrelievi = $dbh->getDettagliPrelievi($_SESSION['admin']);
    require_once 'modals.php';
    // var_dump($_SESSION);
    ?>

    <div class="container justify-content-center col-md-9">
      <hr>
      <h3 class="text-center">Home Amministratore</h3>
      <hr>
      <h5 class="text-center">Inserisci un nuovo membro del personale</h5>
      <br>
      <div class="form-group">
        <form id="iP" action="insert_personale.php" method="post">
        <div class="text-center">
        </form>
        <button id="btn-info" type="submit" class="btn btn-outline-primary">Inserisci Personale</button>
        </div>
      </div>

      <hr>
      <h5 class="text-center">Inserisci un nuovo volontario</h5>
      <br>
      <div class="form-group">
        <form id="iV" action="insert_donatore.php" method="post">
        <div class="text-center">
        </form>
        <button id="btn" type="submit" class="btn btn-outline-danger">Inserisci Volontario</button>
        </div>
      </div>

      <hr>
      <h5 class="text-center">Visualizza turni lavorativi dei medici</h5>
      <br>
        <div class="text-center">
          <button id="btn-info" type="button" class="btn btn-outline-secondary" data-toggle="modal" data-target="#modal2">Visualizza Turni Medici</button>
        </div>

        <hr>
        <h5 class="text-center">Visualizza turni lavorativi degli infermieri</h5>
        <br>
          <div class="text-center">
            <button id="btninf" type="button" class="btn btn-outline-secondary" data-toggle="modal" data-target="#modal3">Visualizza Turni Infermieri</button>
          </div>
          <hr>
          <h5 class="text-center">Visualizza tutti i dettagli prelievi</h5>
          <br>
            <div class="text-center">
              <button id="btn-info" type="button" class="btn btn-outline-secondary" data-toggle="modal" data-target="#modal1">Visualizza Prelievi</button>
            </div>
          <br>
          <h6 class="text-center">Nella tua sede, quest'anno, sono stati eseguiti <?php echo($numPrel[0]['tot']); ?> prelievi.</h6>
          <br>
    </div>

  </body>
</html>
