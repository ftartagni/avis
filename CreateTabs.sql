create table sedi (
	idSede int(3) not null primary key,
	nome varchar(30) not null,
	via varchar(30) not null,
	numero int(4) not null,
	città varchar(30) not null
);

create table locali(
	numeroLocale int(4) not null,
	tipoLocale varchar(30) not null check(tipoLocale in ('Ambulatorio', 'Sala Donazioni', 'Altro'))
	idSede int(3) foreign key references sedi
		on delete set null
		on update no action,
	primary key (numeroLocale, idSede)
);

create table personale (
	CF char(16) not null check(len(CF) = 16) primary key,
  matricola int(3) not null check(matricola>0),
  ruolo varchar(30) not null check(ruolo in ('Medico', 'Infermiere')),
  nome varchar(30) not null,
  cognome varchar(30) not null,
  dataDiNascita date not null,
  telefono bigint(10) not null check(len(telefono) = 10),
  compensoOrario int(2) not null check(compensoOrario>=0),
  idSede int(3) foreign key references sedi
    on delete set null
    on update no action,
  unique(idSede, matricola));

	create table volontari (
		CF char(16) not null check(len(CF) = 16) primary key,
	  nome varchar(30) not null,
	  cognome varchar(30) not null,
	  dataDiNascita date not null,
	  telefono bigint(10) not null check(len(telefono) = 10),
	  idSede int(3) foreign key references sedi
			on delete set null
			on update no action,
		gruppoSanguigno char(3) not null check(gruppoSanguigno in ('0-', '0+',
			 															'A-', 'A+', 'B-', 'B+', 'AB-', 'AB+')),
		frequenzaDonazSangue int(3) not null,
		frequenzaDonazPlasma int(3) not null
	);


create table prestazioni_mediche(
		idPrestazione int(2) not null primary key,
		nomePrestazione varchar(30) not null
	);

create table turni(
  CF char(16) foreign key references personale,
  idSede int(3) not null,
  numeroLocale int(3) not null,
  data date not null,
  orarioInizio time not null,
  orarioFine time not null,
  primary key (idSede, numeroLocale, data),
  unique(CF, data),
  foreign key (idSede, numeroLocale) references locali
    on delete set null
    on update no action
);

create table turni_infermieri(
  CF char(16) foreign key references personale,
  idSede int(3) not null,
  numeroLocale int(3) not null,
  data date not null,
  primary key (idSede, numeroLocale, data),
  unique(CF, data),
  foreign key (idSede, numeroLocale) references locali
    on delete set null
    on update no action
);

create table dettagli_prelievi(
  idDettaglioPrelievo int not null primary key,
  idPrestazione int(2) not null foreign key references prestazioni_mediche
    on delete set null
    on updato no action,
  idSede int(3) not null,
  numeroLocale int(3) not null,
  data date not null,
  oraInizio time not null,
	qtà_prelevata int not null,
  CF char(16) foreign key references volontari
    on delete set null
    on update no action,
  importoColazione int(2),
  note varchar,
  foreign key (idSede, numeroLocale, data) references turni
  on delete set null
  on update no action,
  unique (CF, oraInizio, numeroLocale),
  unique (idSede, numeroLocale, data, oraInizio)
);

create table dettagli_visite(
  idDettaglioVisita int not null primary key,
  idPrestazione int(2) not null foreign key references prestazioni_mediche
    on delete set null
    on updato no action,
  idSede int(3) not null,
  numeroLocale int(3) not null,
  data date not null,
  oraInizio time not null,
  CF char(16) foreign key references volontari
    on delete set null
    on update no action,
  esito int(1) not null,
  emoglobina int(3) not null check (emoglobina>0 and emoglobina<100),
  note varchar,
  foreign key (idSede, numeroLocale, data) references turni
  on delete set null
  on update no action,
  unique (CF, oraInizio, numeroLocale),
  unique (idSede, numeroLocale, data, oraInizio)
);

create table oggetti_medici(
  idOggettoMedico int(3) not null primary key,
  nome varchar(30) not null,
  costoUnitario int not null
);

create table consumi(
  idDettaglioPrelievo int not null foreign key references dettagli_prelievi
    on delete set null
    on updato no action,
  idOggettoMedico int(3) not null foreign key references oggetti_medici
    on delete set null
    on updato no action,
  quantità int(2) not null check(quantità>0),
  primary key (idDettaglioPrelievo, idOggettoMedico)
);
