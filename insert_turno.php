<!DOCTYPE html>
<html lang="it">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <title>Avis - Inserisci turno</title>
    <!-- <link href="style.css" rel="stylesheet"> -->
  </head>
  <body>
    <?php require_once 'functions.php'; ?>
    <?php require_once 'navbar_home.php';
    require_once 'bootstrap.php';
    sec_session_start();
    // require_once 'modals.php';
    $sedi = $dbh->getSedi();
    $volontari = $dbh->getVolontari();
    // var_dump($_SESSION);
    ?>
    <style media="screen">
      label{
        margin-top: 2%;
        margin-bottom: 1%;
      }
    </style>
    <nav aria-label="breadcrumb" style="margin-top: 1%;">
        <ol class="breadcrumb bg-light">
            <li class="breadcrumb-item"><a href="home_medico.php">Home Medico</a></li>
            <li class="breadcrumb-item active" aria-current="page">Inserisci Turno</li>
        </ol>
    </nav>
  <div class="container justify-content-center col-md-4">
    <h3 class="text-center">Inserimento turno di lavoro </h3>
  <hr class="upRegister">
  <div class="form-group">
    <form id="form-turno" action="insert_turno_function.php" method="post">

      <label for="date">Data</label>
      <input type="date" class="form-control" name="date" id="date" placeholder="Data" required>

      <label for="oraI">Ora di inizio</label>
      <input type="time" class="form-control" name="oraI" id="oraI" placeholder="Ora di inizio" required>

      <label for="oraF">Ora di fine</label>
      <input type="time" class="form-control" name="oraF" id="oraF" placeholder="Ora di fine" required>

      <label for="sede">Sede</label>
      <select name="sede"  class="form-control" style="display: inline-block; margin-top: 2%;">
        <?php foreach($sedi as $sede): ?>
          <option value="<?php echo $sede['idSede']; ?>"><?php echo $sede['nome']; ?></option>
        <?php endforeach; ?>
      </select>
      <br>
      <label for="locale">Numero Locale</label>
      <select name="locale"  class="form-control" style="display: inline-block;">
        <option value="1">1</option>
        <option value="2">2</option>
      </select>
      <br>
      <br>
      <button type="submit" class="btn btn-primary" style="display: block;">Conferma</button>
    </form>
  </div>
  </div>

</body>
</html>
