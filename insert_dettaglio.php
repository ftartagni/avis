<!DOCTYPE html>
<html lang="it">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <title>Avis - Inserisci prelievo</title>
    <!-- <link href="style.css" rel="stylesheet"> -->
  </head>
  <body>
    <?php require_once 'functions.php'; ?>
    <?php require_once 'navbar_home.php';
    require_once 'bootstrap.php';
    // require_once 'modals.php';
    $sedi = $dbh->getSedi();
    $volontari = $dbh->getVolontari();
    ?>
    <style media="screen">
      label{
        margin-top: 2%;
        margin-bottom: 1%;
      }
    </style>
    <nav aria-label="breadcrumb" style="margin-top: 1%;">
        <ol class="breadcrumb bg-light">
            <li class="breadcrumb-item"><a href="home_medico.php">Home Medico</a></li>
            <li class="breadcrumb-item active" aria-current="page">Inserisci Nuovo Prelievo</li>
        </ol>
    </nav>
  <div class="container justify-content-center col-md-4">
    <h3 class="text-center">Inserimento prelievo </h3>
  <hr class="upRegister">
  <div class="form-group">
    <form id="form-registrazione" action="insert_dettaglio_function.php" method="post">
      <label for="qta_prel">Quantità prelevata (in mL)</label>
      <input type="text" class="form-control" name="qta_prel" id="qta_prel" placeholder="Quantità prelevata" maxlength="3" required>

      <!-- <label for="CF">Codice Fiscale</label>
      <input type="text" class="form-control" name="CF" id="CF" placeholder="Codice Fiscale" maxlength="16" required> -->
      <label for="volontario">CF volontario</label>
      <select name="volontario"  class="form-control">
        <?php foreach($volontari as $volontario): ?>
          <option value="<?php echo $volontario['CF'];?>"><?php echo $volontario['CF'];?></option>
        <?php endforeach; ?>
      </select>
      <label for="date">Data</label>
      <input type="date" class="form-control" name="date" id="date" placeholder="Data" required>

      <label for="ora">Ora di inizio</label>
      <input type="time" class="form-control" name="ora" id="ora" placeholder="Ora di inizio" required>

      <label for="sede">Sede</label>
      <select name="sede"  class="form-control" style="display: inline-block; margin-top: 2%;">
        <?php foreach($sedi as $sede): ?>
          <option value="<?php echo $sede['idSede']; ?>"><?php echo $sede['nome']; ?></option>
        <?php endforeach; ?>
      </select>
    <!-- </br> -->
      <label for="tipo">Tipo di Prelievo</label>
      <select name="tipo" class="form-control" style="display: inline-block;">
        <option value="2">Prelievo di Sangue</option>
        <option value="3">Prelievo del Plasma</option>
      </select>
      <br>
      <label for="locale">Numero Locale</label>
      <select name="locale"  class="form-control" style="display: inline-block;">
        <option value="1">1 Ambulatorio San Mauro</option>
        <option value="2">2 Sala Donazioni San Mauro</option>
        <option value="3">3 Ambulatorio Savignano</option>
        <option value="4">4 Sala Donazioni Savignano</option>
        <option value="5">5 Ambulatorio Gambettola</option>
        <option value="6">6 Sala Donazioni Gambettola</option>
      </select>
      <!-- <div class="form-group"> -->
    <label for="note">Note</label>
    <textarea class="form-control" name="note" id="note" rows="3"></textarea>
      <!-- </div> -->
      <label for="imp">Importo Colazione</label>
      <input type="number" class="form-control" name="imp" id="imp" placeholder="Importo Colazione" maxlength="3" required>
      <br>
      <button type="submit" class="btn btn-primary" style="display: block;">Conferma</button>
    </form>
  </div>
  </div>

</body>
</html>
