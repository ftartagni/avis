INSERT INTO personale VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)    --uno in più per matricola

INSERT INTO volontari VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)

INSERT INTO turni VALUES (?, ?, ?, ?, ?, ?)

INSERT INTO dettagli_visite VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)

INSERT INTO dettagli_prelievi VALUES (?, ?, ?, ?, ?, ?, ?, ?)

INSERT INTO consumi VALUES (?, ?, ?)

--TURNI MEDICI
SELECT t.CF, p.nome, p.cognome, t.data, t.orarioInizio, t.orarioFine
 FROM personale p, turni t
 WHERE p.CF = t.CF
 AND p.idSede = ?

--TUTTE VISITE DI UNA SEDE ok
SELECT d.idDettaglioVisita, d.data, d.oraInizio, d.esito, d.emoglobina,
       v.nome, v.cognome
 FROM dettagli_visite d, volontari v
 WHERE d.CF = v.CF
 AND d.idSede = v.idSede
 AND d.idSede = ?

--TUTTI PRELIEVI DI UNA SEDE ok
 SELECT d.idDettaglioPrelievo, d.idPrestazione, d.data, d.oraInizio, d.qta_prelevata,
        d.note, v.nome, v.cognome, p.nomePrestazione
  FROM dettagli_prelievi d, volontari v, prestazioni_mediche p
  WHERE d.CF = v.CF
  AND d.idSede = v.idSede
  AND d.idSede = ?
  AND d.idPrestazione = p.idPrestazione

--SPESE PER COLAZIONI (IN MESE?) ok
 SELECT SUM(d.importoColazione), d.data
  FROM dettagli_prelievi d
  WHERE d.idSede = ?
  AND MONTH(d.data) = MONTH(DATE())

--COMPENSO MENSILE MEDICO
SELECT SUM(TIMESTAMPDIFF(HOUR, t.orarioInizio, t.orarioFine)*p.compensoOrario) AS compenso_mensile
FROM turni t, personale p
WHERE t.CF = p.CF
AND t.CF = 'TRTFPP98R30C573G' --'?'
AND MONTH(t.data) = MONTH(CURDATE()) --date

--NUMERO ORE LAVORATE IN UN MESE DA UN MEDICO
SELECT SUM(TIMESTAMPDIFF(HOUR, t.orarioInizio, t.orarioFine)) AS ore_lavorate
FROM turni t, personale p
WHERE t.CF = p.CF
AND t.CF = ?
AND MONTH(t.data) = MONTH(DATE(?))


SELECT SUM(o.costoUnitario * c.quantita) AS 'spese_oggetti'
 FROM consumi c, oggetti_medici o, dettagli_prelievi d
 WHERE MONTH(d.data) = ?
 AND c.idDettaglioPrelievo = d.idDettaglioPrelievo
 AND o.idOggettoMedico = c.idOggettoMedico
 AND d.idSede = ?

--TUTTE LE VISITE DI UN PAZIENTE (sia per medici che volontari)
SELECT d.idDettaglioVisita, d.data, d.oraInizio, d.esito, d.emoglobina,
       v.nome, v.cognome
 FROM dettagli_visite d, volontari v
 WHERE d.CF = v.CF
 AND d.idSede = v.idSede
 AND v.CF = ?

 --TUTTI I PRELIEVI DI UN PAZIENTE (sia per medici che volontari)
 SELECT d.idDettaglioPrelievo, d.idPrestazione, d.data, d.oraInizio, d.qta_prelevata,
        d.note, v.nome, v.cognome, p.nomePrestazione
  FROM dettagli_prelievi d, volontari v, prestazioni_mediche p
  WHERE d.CF = v.CF
  AND d.idSede = v.idSede
  AND d.idPrestazione = p.idPrestazione
  AND v.CF = ?

--VISUALIZZAZIONE FREQUENZA DONAZIONI ok
SELECT v.frequenzaDonazPlasma, v.frequenzaDonazSangue
 FROM volontari v
 WHERE v.CF = ?

--NUMERO PRELIEVI TOTALI ANNUALI IN UNA SEDE ok
SELECT COUNT(*)
 FROM dettagli_prelievi d, sedi s
 WHERE d.idSede = s.idSede
 AND YEAR(d.data) = ?
 AND d.idSede = ?

 --VEDIAMO (TUTTI I TURNI INFERMIERI IN UNA SEDE)
 SELECT ti.CF, p.nome, p.cognome, t.data, t.orarioInizio, t.orarioFine, t.CF
  FROM personale p, turni t, turni_infermieri ti
  WHERE p.CF = ti.CF
  AND ti.idSede = t.idSede
  AND ti.numeroLocale = ti.numeroLocale
  AND ti.data = t.data
  AND p.idSede = ?

--COMPENSO INFERMIERI MENSILE
SELECT ti.CF, p.nome, p.cognome, t.data, t.orarioInizio, t.orarioFine, t.CF,
 SUM(TIMESTAMPDIFF(HOUR, t.orarioInizio, t.orarioFine)*p.compensoOrario) AS compenso_mensile
 FROM personale p, turni t, turni_infermieri ti
 WHERE p.CF = ?
 AND ti.idSede = t.idSede
 AND ti.numeroLocale = ti.numeroLocale
 AND ti.data = t.data
 AND p.CF = ti.CF
 AND p.idSede = ?
 AND MONTH(ti.data) = ?
