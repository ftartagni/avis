<!DOCTYPE html>
<html lang="it">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <title>Avis - Inserisci personale</title>
    <!-- <link href="style.css" rel="stylesheet"> -->
  </head>
  <body>
    <?php require_once 'functions.php'; ?>
    <?php require_once 'navbar_home.php';
    require_once 'bootstrap.php';
    // require_once 'modals.php';
    $sedi = $dbh->getSedi();
    ?>
    <nav aria-label="breadcrumb" style="margin-top: 1%;">
        <ol class="breadcrumb bg-light">
            <li class="breadcrumb-item"><a href="home_admin.php">Home Amministratore</a></li>
            <li class="breadcrumb-item active" aria-current="page">Inserisci Volontario</li>
        </ol>
    </nav>
    <style media="screen">
      label{
        margin-top: 2%;
        margin-bottom: 1%;
      }
    </style>
  <div class="container justify-content-center col-md-4">
    <h3 class="text-center">Inserimento Personale </h3>
  <hr class="upRegister">
  <div class="form-group">
    <form id="form-registrazione" action="insert_personale_function.php" method="post">
      <label for="ruolo">Ruolo</label>
      <select class="form-control" name="ruolo" style="display: inline-block; margin-top: 2%;">
          <option value="Infermiere">Infermiere</option>
          <option value="Medico">Medico</option>
      </select>
      <br>
      <label for="nome">Nome</label>
      <input type="text" class="form-control" name="nome" id="nome" placeholder="Nome" maxlength="30" required>

      <label for="cognome">Cognome</label>
      <input type="text" class="form-control" name="cognome" id="cognome" placeholder="Cognome" maxlength="30" required>

      <label for="CF">Codice Fiscale</label>
      <input type="text" class="form-control" name="CF" id="CF" placeholder="Codice Fiscale" maxlength="16" required>

      <label for="tel">Telefono</label>
      <input type="tel" class="form-control" name="tel" id="tel" placeholder="Telefono" maxlength="10" required>

      <label for="date">Data di nascita</label>
      <input type="date" class="form-control" name="birthdate" id="birthdate" placeholder="Data di nascita" max="2002-02-14" required>

      <label for="compenso">Compenso Orario</label>
      <input type="text" class="form-control" name="compenso" id="compenso" placeholder="Compenso Orario" maxlength="2" required>

      <label for="sede">Sede</label>
      <select class="form-control" name="sede" style="display: inline-block; margin-top: 2%;">
        <?php foreach($sedi as $sede): ?>
          <option value="<?php echo $sede['idSede']; ?>"><?php echo $sede['nome']; ?></option>
        <?php endforeach; ?>
      </select>
      <br>
      <br>
      <button type="submit" class="btn btn-primary" style="display: block;">Conferma</button>
    </form>
  </div>
  </div>

</body>
</html>
