<!DOCTYPE html>
<html lang="it">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <title>Homepage - medico</title>
    <!-- <link href="style.css" rel="stylesheet"> -->
  </head>
  <body>
    <?php require_once 'functions.php';
    require_once 'bootstrap.php';?>
    <?php sec_session_start();
    if(!empty($_POST['medico'])){
      $_SESSION['medico'] = $_POST['medico'];
    }
    //var_dump($_SESSION['medico']); ?>
    <?php require_once 'navbar_home.php';
    //sec_session_start();?>

    <div class="container justify-content-center col-md-9">
      <hr>
        <h3 class="text-center">Home medico</h3>
      <hr>
      <h5 class="text-center">Inserisci i dettagli di un prelievo eseguito</h5>
      <br>
      <div class="form-group">
        <form id="iD" action="insert_dettaglio.php" method="post">
        <div class="text-center">
        </form>
        <button id="btn-info" type="submit" class="btn btn-outline-danger">Inserisci Prelievo</button>
        </div>
      </div>

      <hr>
      <h5 class="text-center">Inserisci gli orari del tuo turno</h5>
      <br>
      <div class="form-group">
        <form id="iT" action="insert_turno.php" method="post">
        <div class="text-center">
        </form>
        <button id="btn-info" type="submit" class="btn btn-outline-primary">Inserisci Turno</button>
        </div>
      </div>

    </div>





  </body>
</html>
